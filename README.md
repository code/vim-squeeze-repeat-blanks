squeeze\_repeat\_blanks.vim
===============================

This plugin provides a user command to reduce multiple blank lines to the last
blank line in that group, for the given range or the whole buffer by default.
What constitutes a "blank" line is configurable per-buffer.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
